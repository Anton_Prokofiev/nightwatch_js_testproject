module.exports = {
    '@tags' : ['google'],
    'Google advanced search: Elon Musk'(browser){
        const searchQuery = 'Elon Musk';
        const page = browser.page.googleAdvancedSearch();

        const resultsPageQuerySelector = `#searchform input[name="q"][value="${searchQuery}`;
        const resultPageLanguageSelector = '(//*[@class="hdtb-mn-hd hdtb-tsel"])[1]';
        const resultPageLastUpdateSelector = '(//*[@class="hdtb-mn-hd hdtb-tsel"])[2]';

        page
            .navigate()
            .setQuery(searchQuery)
            .selectFilter("@languageDropDownMenu", "lang_it")
            .selectFilter("@lastUpdateDropDownMenu", "m")
            .search()
            .assert.urlContains('as_q=Elon+Musk', 'Search query is Elon Musk');

        browser
            .assert.visible(resultsPageQuerySelector, 'UI: Elon Musk is set in a query input')
            .useXpath()
            .assert.attributeContains(resultPageLanguageSelector, 'aria-label', 'Шукати сторінки такою мовою: італійська', "UI: language is set to Italian")
            .assert.attributeContains(resultPageLastUpdateSelector, 'aria-label', ' За минулий місяць', "UI: last update is set to last month")

        browser.saveScreenshot('tests_output/google.png');
    }
}