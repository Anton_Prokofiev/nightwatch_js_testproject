module.exports = {
    url: 'https://www.google.com/advanced_search',
    elements: {
         searchQueryInputField: 'input[name="as_q"]',
         languageDropDownMenu: '#lr_button',
         lastUpdateDropDownMenu: '#as_qdr_button',
         submitButton: '.jfk-button[type="submit"]',
    },
    commands: [{
        setQuery(value){
            return this
                .setValue('@searchQueryInputField', value)
        },
         selectFilter(selector, value){
             return this
                .click(selector)
                .click(`.goog-menuitem[value="${value}"]`);
         },
         search() {
            return this
                .click("@submitButton");
         }
    }]
};