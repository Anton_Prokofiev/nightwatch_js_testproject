const chrome = require('chromedriver')

module.exports = {
  src_folders: ['tests'],
  page_objects_path: ['C:/Users/prokofiev/Documents/JSNightwatch/page-objects'],
  webdriver: {
    start_process: true,
    server_path: chrome.path,
    port: 9515,
  },
  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: 'chrome',
      },
    },
  },
}